all:
	latexmk -xelatex -shell-escape cv 
	# pdflatex -shell-escape paper
	# bibtex paper
	# pdflatex -shell-escape paper
	# pdflatex -shell-escape paper

clean:
	latexmk -c
	rm -rf paper.pdf paper.bbl paper.xdv paper.dvi images/*-eps-converted-to.pdf

